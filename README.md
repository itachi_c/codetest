# CodeTest
Prerequisite:
DB: MYSQL 
	config: {
		username: "root",
		password: "password",
		port: 3306
	}
MYSQL installer download link: https://dev.mysql.com/downloads/windows/installer/8.0.html

IDE: Visual Studio Code

Android Studio for emulating the Android device
download link: https://developer.android.com/studio/?gclid=Cj0KCQjwsuP5BRCoARIsAPtX_wGGB6r_XPeskwkibImYg3hmyNBpXm9pPMIH1UJtKObw0xBlahIflKIaAmEIEALw_wcB&gclsrc=aw.ds
Creating Android Virtual Device (AVD) Guide: https://developer.android.com/studio/run/managing-avds

NodeJs version: 10.16.3 above
NodeJs download link: https://nodejs.org/en/

After clone project:

Step 1: Use Workbench open \codetest\DB\codeTest.mwb

Step 2: forward engineering codeTest.mwb 

Step 3: Open \codetest\ with Visual Studio Code

Step 4: "> cd .\application\" and "> npm install"

Step 5: open another terminal tab "> cd .\server\" and "> npm install"

Step 6: go to application\environment.ts and change the "192.168.1.133" to the ip address of the hosting of express server 

Step 7: "> npm start" at the \application\ directory and "node index.js" at the \server\ directory

For Web:
	Step 8: Click the "Run in Web Browser" (Windows not support date picker)

For Android:
	Step 8: Install Expo application to the AVD and Click the "Run on Android device/emulator"
	
PS. If any code modified in \server\index.ts, run "> npm run tsc " before restart the serve.