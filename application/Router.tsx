import React from 'react'
import { Router, Scene, Stack, Overlay, Lightbox, Actions } from 'react-native-router-flux'
import LoginPage from './login';
import RegisterPage from './register';
import { StyleSheet, Button, TouchableOpacity, Text, View } from 'react-native';
import ListPage from './consultationList';
import RecordPage from './record';

const logoutButton = () => {
   return (
      <TouchableOpacity
         style={styles.logoutButton}
         onPress={() => Actions.login()}>
         <Text >
            Logout
         </Text>
      </TouchableOpacity>
   )
}

const Routes = () => (
   <Router sceneStyle={styles.child}>
      <Stack key="root" >
         <Scene
            hideNavBar
            key="login"
            sceneStyle={styles.child}
            component={LoginPage}
            title="Login"
            initial={true} />
         <Scene
            key="register"
            sceneStyle={styles.child}
            component={RegisterPage}
            title="Register" />

         <Scene
            renderRightButton={logoutButton}
            renderBackButton={() => <View />}
            key="list"
            sceneStyle={styles.child}
            component={ListPage}
            title="Consultation List" />

         <Scene
            key="record"
            sceneStyle={styles.child}
            component={RecordPage}
            title="Record" />
      </Stack >
   </Router >
)

const styles = StyleSheet.create({
   child: {
      height: "100%"
   },
   logoutButton: {
      height: "100%",
      width: 64,
      justifyContent: "center",
      alignItems: "center"
   }
})

export default Routes