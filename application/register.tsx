import { View, StyleSheet, TextInput, TouchableOpacity, Text, SafeAreaView } from "react-native";
import React, { useState } from 'react';
import { Actions } from "react-native-router-flux";
import { Environment } from "./environment";

type RegisterState = {
    registerBody: {
        email: string,
        password: string,
        clinicName: string,
        phoneNo: string,
        address: string,
    },
    confirmPassword: string,
    addressRoom: string,
    addressFloor: string,
    addressBuilding: string,
    addressStreet: string,
    addressCity: string,
    addressState: string,
    alertMessage: string,
    successMessage: string
}

class RegisterPage extends React.Component<{}, RegisterState> {
    constructor(props:{}) {
        super(props)
        this.state = {
            registerBody: {
                email: "",
                password: "",
                clinicName: "",
                phoneNo: "",
                address: "",
            },
            confirmPassword: "",
            addressRoom: "",
            addressFloor: "",
            addressBuilding: "",
            addressStreet: "",
            addressCity: "",
            addressState: "",
            alertMessage: "",
            successMessage: ""
        }
    }

    emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    back = () => {
        Actions.pop();
    }

    submit = () => {
        if (this.emailReg.test(this.state.registerBody.email)) {
            if (this.checkPW()) {
                this.setState({ alertMessage: "" });
                let addressArray = [this.state.addressRoom,
                this.state.addressFloor,
                this.state.addressBuilding,
                this.state.addressStreet,
                this.state.addressCity,
                this.state.addressState];
                addressArray = addressArray.filter(o => o.length > 0);
                this.state.registerBody.address = addressArray.join(', ')
                fetch(`http://${Environment.expressUrl}/register`, {
                    method: "POST",
                    headers: {
                        Accept: 'application/json',
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify(this.state.registerBody)

                })
                    // .then((response) => response.json())
                    .then((res) => {
                        if (res) {
                            this.setState({ successMessage: "Register Success, Will Back to Login Page in 3 Seconds" });
                            setTimeout(() => {
                                this.back();
                            }, 3000);

                        }
                    }).catch(error => {
                        this.setState({ alertMessage: typeof error === "string" ? error : "Register Error" });
                    })
            }


        } else {
            this.setState({ alertMessage: "Wrong Email Format" });
        }
    }


    updateRegisterBodyValue = (field: string, value: any) => {
        this.setState(
            {
                registerBody: {
                    ...this.state.registerBody,
                    [field]: value
                }
            }
        )
    }

    checkPW = () => {
        if (this.state.registerBody.password == this.state.confirmPassword && this.state.registerBody.password.length > 0) {
            return true
        } else {
            this.state.registerBody.password.length > 0 && this.state.confirmPassword.length > 0 ?
                this.setState({ alertMessage: "Password and Confirm Password not match" }) :
                this.setState({ alertMessage: "Password and Confirm Password cannot empty" });
            return false
        }
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder='Login Email'
                        style={styles.first}
                        onChangeText={text => this.updateRegisterBodyValue('email', text)}
                        value={this.state.registerBody.email}
                        autoCorrect={false}
                    />
                    <TextInput
                        placeholder='Password'
                        textContentType="password"
                        secureTextEntry={true}
                        style={styles.input}
                        onChangeText={text => this.updateRegisterBodyValue('password', text)}
                        value={this.state.registerBody.password}
                        autoCorrect={false}
                    />
                    <TextInput
                        placeholder='Confirm Password'
                        secureTextEntry={true}
                        style={styles.input}
                        onChangeText={text => this.setState({ confirmPassword: text })}
                        value={this.state.confirmPassword}
                        autoCorrect={false}
                    />
                    <TextInput
                        placeholder='Clinic Name'
                        style={styles.input}
                        onChangeText={text => this.updateRegisterBodyValue('clinicName', text)}
                        value={this.state.registerBody.clinicName}
                        autoCorrect={false}
                    />
                    <TextInput
                        placeholder='Phone Number'
                        textContentType="telephoneNumber"
                        style={styles.input}
                        onChangeText={text => this.updateRegisterBodyValue('phoneNo', text)}
                        value={this.state.registerBody.phoneNo}
                        autoCorrect={false}
                    />
                    <View style={styles.addressInputContainer}>
                        <View style={styles.addressRow}>
                            <TextInput
                                placeholder='Room'
                                style={styles.addressInputMulti}
                                onChangeText={text => this.setState({ addressRoom: text })}
                                value={this.state.addressRoom}
                                autoCorrect={false}
                            />
                            <TextInput
                                placeholder='Floor'
                                style={styles.addressInputMulti}
                                onChangeText={text => this.setState({ addressFloor: text })}
                                value={this.state.addressFloor}
                                autoCorrect={false}
                            />
                        </View>
                        <TextInput
                            placeholder='Building'
                            style={styles.addressInput}
                            onChangeText={text => this.setState({ addressBuilding: text })}
                            value={this.state.addressBuilding}
                            autoCorrect={false}
                        />
                        <TextInput
                            placeholder='Street'
                            style={styles.addressInput}
                            onChangeText={text => this.setState({ addressStreet: text })}
                            value={this.state.addressStreet}
                            autoCorrect={false}
                        />
                        <TextInput
                            placeholder='City'
                            style={styles.addressInput}
                            onChangeText={text => this.setState({ addressCity: text })}
                            value={this.state.addressCity}
                            autoCorrect={false}
                        />
                        <TextInput
                            placeholder='State'
                            style={styles.addressInput}
                            onChangeText={text => this.setState({ addressState: text })}
                            value={this.state.addressState}
                            autoCorrect={false}
                        />

                    </View>


                    <View
                        style={styles.alertMessageContainer}>
                        <Text style={styles.alertMessage}>
                            {this.state.alertMessage}
                        </Text>
                        <Text style={styles.successMessage}>
                            {this.state.successMessage}
                        </Text>
                    </View>
                    <View style={styles.buttonContainer}
                    >
                        <TouchableOpacity
                            style={styles.backButton}
                            onPress={this.back}
                        >
                            <Text style={styles.buttonText}>
                                Back
                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.submitButton}
                            onPress={this.submit}
                        >
                            <Text style={styles.buttonText}>
                                Submit
                        </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}








const styles = StyleSheet.create({
    container: {
        height: "100%",
        flex: 1,
        backgroundColor: "#e1e1e1",
        alignItems: "center",
        justifyContent: 'center'
    },
    inputContainer: {
        width: "60%"
    },
    first: {
        fontSize: 18,
        height: 32,
        borderWidth: 1,
        paddingHorizontal: 8
    },
    input: {
        marginTop: 24,
        fontSize: 18,
        height: 32,
        borderWidth: 1,
        paddingHorizontal: 8
    },
    addressInputContainer: {
        marginTop: 16,
    },

    addressRow: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    addressInputMulti: {
        width: "48%",
        marginTop: 8,
        fontSize: 18,
        height: 32,
        borderWidth: 1,
        paddingHorizontal: 8
    },
    addressInput: {
        marginTop: 8,
        fontSize: 18,
        height: 32,
        borderWidth: 1,
        paddingHorizontal: 8
    },

    alertMessageContainer: {
        marginTop: 8,
        justifyContent: "flex-start",
    },
    alertMessage: {
        color: "#AA3333",
        fontSize: 12,
    },
    successMessage: {
        color: "#347b23",
        fontSize: 12,
    },
    buttonContainer: {
        marginTop: 24,
        flexDirection: "row",
        justifyContent: "space-around"
    },
    backButton: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        height: 40,
        backgroundColor: "#e63333",
    },
    submitButton: {
        marginLeft: 16,
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        height: 40,
        backgroundColor: "#0072bb",
    },
    buttonText: {
        color: "#fff",
        fontSize: 16
    }
})

export default RegisterPage