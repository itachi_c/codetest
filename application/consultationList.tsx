import React, { useState } from 'react';
import { Actions } from 'react-native-router-flux';
import { View, StyleSheet, Text, TouchableOpacity, SafeAreaView, FlatList, ListRenderItem, ScrollView } from 'react-native';
import { ConsultationRecord } from './consultation-record';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Environment } from './environment';

type ListProps = {
    userInfo: {
        clinicId: string;
        email: string;
        clinicName: string;
        phoneNo: string;
        address: string;
    };
}

type ListState = {
    returnList: Array<ConsultationRecord>;
    isRefreshing: boolean;
    totalRecord: number;
    pagingMode: string;
    isPickDate: boolean;
    queryParameters: {
        [key: string]: string | null | number
        recordDateFrom: string | null,
        recordDateTo: string | null,
        ordering: string | null,
        offset: number,
        limit: number
    }
}

class ListPage extends React.Component<ListProps, ListState> {

    constructor(props: ListProps) {
        super(props)
        this.state = {
            returnList: [],
            isRefreshing: false,
            totalRecord: 0,
            pagingMode: "daily",
            isPickDate: false,
            queryParameters: {
                recordDateFrom: null,
                recordDateTo: null,
                ordering: null,
                offset: 0,
                limit: 10
            }
        }
    }

    consultationList: Array<ConsultationRecord> = [];


    componentDidMount() {
        this.getConsultationRecord();
    }

    getConsultationRecord = () => {
        this.setState({ isRefreshing: true });
        let url = new URL(`http://${Environment.expressUrl}/consultation-records`)
        Object.keys(this.state.queryParameters).forEach(
            key => {
                if (this.state.queryParameters[key] != null) {
                    url.searchParams.append(key,
                        String(this.state.queryParameters[key]).toString())
                }
            }
        )
        fetch(url.toString(), {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'content-type': 'application/json',
                user_id: this.props.userInfo.clinicId
            },

        })
            .then((response) => response.json())
            .then(res => {

                this.setState({
                    totalRecord: res.total,
                    returnList: this.state.returnList.concat(res.results),
                    queryParameters: {
                        ...this.state.queryParameters,
                        offset: this.state.returnList.length
                    }
                });

            }).catch(error => {

            }).finally(
                () => {
                    this.setState({ isRefreshing: false });
                }
            )
    }

    newRecord = () => {
        Actions.record({
            userInfo: this.props.userInfo
        });
    }

    viewDetail = (record: ConsultationRecord) => {
        Actions.record({
            userInfo: this.props.userInfo,
            record: record
        })
    }

    refreshList = () => {
        this.setState({
            queryParameters: {
                ...this.state.queryParameters,
                offset: 0
            }
        })
        this.setState({ returnList: [] })
        this.getConsultationRecord();
    }

    onGetMore = () => {
        if (this.state.totalRecord > this.state.returnList.length) {
            this.setState({
                queryParameters: {
                    ...this.state.queryParameters,
                    offset: this.state.returnList.length
                }
            })
            this.getConsultationRecord();
        }
    }

    // showDate = (dateTime:string) =>{
    //     return new Date(dateTime).toDateString()
    // }

    setMode = (mode: string) => {
        this.setState({ pagingMode: mode });
        if (mode != 'daily') {
            this.setState({ isPickDate: true })
        } else {
            this.setState({
                queryParameters: {
                    ...this.state.queryParameters,
                    recordDateFrom: null,
                    recordDateTo: null
                }
            })
            this.refreshList();
        }
    }

    selectDate = (event: any, selectedDate?: Date) => {
        this.setState({ isPickDate: false })
        if (selectedDate) {
            let endDate = new Date(selectedDate);
            if (this.state.pagingMode === "weekly") {
                endDate.setDate(selectedDate.getDate() + 7)
            } else {
                endDate.setMonth(selectedDate.getMonth() + 1)
            }
            this.setState(
                {
                    queryParameters: {
                        ...this.state.queryParameters,
                        recordDateFrom: selectedDate?.toISOString().slice(0, 10) + " 00:00:00",
                        recordDateTo: endDate.toISOString().slice(0, 10) + " 23:59:59",
                    }
                }
            )
            this.refreshList();
        }

    }

    listItem = ({ item }: { item: ConsultationRecord }) => {
        return (
            <TouchableOpacity
                onPress={() => this.viewDetail(item)}
                style={styles.itemContainer}>
                <View style={styles.itemRow}>
                    <View style={styles.itemLeft}>
                        <Text style={styles.doctor} >
                            {item.doctorName}
                        </Text>
                        <Text style={styles.patient}>
                            {item.patientName} {item.needFollowup === 0 ? "(Need Followup)" : ''}
                        </Text>
                    </View>

                    <View style={styles.itemRight}>
                        <Text style={styles.fee}>
                            $ {item.consultationFee}
                        </Text>
                    </View>
                </View>
                <View>
                    <Text style={styles.itemTime}>
                        {new Date(item.dateTime).toISOString().slice(0, 10)}
                    </Text>
                </View>


            </TouchableOpacity >
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.actionContainer}>
                    <View style={styles.queryMode}>
                        <TouchableOpacity
                            style={styles.dailyMode}
                            onPress={() => this.setMode("daily")}
                        >
                            <Text style={styles.buttonText}>
                                Daily
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.weeklyMode}
                            onPress={() => this.setMode("weekly")}
                        >
                            <Text style={styles.buttonText}>
                                Weekly
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.monthlyMode}
                            onPress={() => this.setMode("monthly")}
                        >
                            <Text style={styles.buttonText}>
                                Monthly
                            </Text>
                        </TouchableOpacity>
                        {this.state.isPickDate ? (<DateTimePicker
                            testID="dateTimePicker"
                            value={new Date()}
                            mode="date"
                            display="default"
                            onChange={this.selectDate}
                        />) : undefined}
                    </View>

                    <TouchableOpacity
                        style={styles.create}
                        onPress={this.newRecord}
                    >
                        <Text style={styles.buttonText}>
                            Create
                        </Text>
                    </TouchableOpacity>
                </View>
                <View>

                </View>
                <View style={styles.listContainer} >
                    {this.state.totalRecord === 0 ?
                        (
                            < View style={styles.empty}>
                                <Text>
                                    No Record
                                </Text>
                            </View>
                        ) :
                        (
                            <FlatList
                                initialNumToRender={8}
                                refreshing={this.state.isRefreshing}
                                onRefresh={() => this.refreshList}
                                data={this.state.returnList}
                                renderItem={this.listItem}
                                keyExtractor={record => record.recordId}
                                onEndReachedThreshold={0.01}
                                onEndReached={this.onGetMore}
                            />
                        )

                    }
                </View >


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create(
    {
        container: {
            flexDirection: "column",
            height: "100%"
        },
        actionContainer: {
            height: 48,
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"

        },
        queryMode: {
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: 'center',
            height: "100%"

        },
        dailyMode: {
            paddingHorizontal: 8,
            height: "80%",
            backgroundColor: "#333",
            marginRight: 8,
            justifyContent: "center",
            alignItems: "center"
        },
        weeklyMode: {
            paddingHorizontal: 8,
            height: "80%",
            backgroundColor: "#444",
            marginRight: 8,
            justifyContent: "center",
            alignItems: "center"

        },
        monthlyMode: {
            paddingHorizontal: 8,
            height: "80%",
            backgroundColor: "#555",
            justifyContent: "center",
            alignItems: "center"

        },
        datePickerContainer: {
            flex: 1,
            flexDirection: "row",
            justifyContent: "flex-start",
            alignContent: "center"
        },
        create: {
            width: 80,
            height: "80%",
            backgroundColor: "#347b23",
            justifyContent: "center",
            alignItems: "center"
        },
        buttonText: {
            color: "#fff",
            fontSize: 16,
        },

        listContainer: {
            flex: 1,
            flexDirection: "column",
            marginVertical: 16
        },
        empty: {
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
        },
        itemContainer: {
            padding: 8,
            marginHorizontal: 8,
            flexDirection: "column",
            justifyContent: "space-around",
            marginTop: 8,
            borderWidth: 1,
            borderRadius: 5,
            borderColor: "#DDD",
            // height: 64
        },
        itemRow: {
            flexDirection: "row",
        },
        itemLeft: {
            flex: 1
        },
        doctor: {
            color: "#00a0e9",
            fontSize: 18

        },
        patient: {
            color: "#333",
            fontSize: 16

        },

        itemRight: {
            flex: 1,
            alignItems: "flex-end",
            justifyContent: "center"
        },
        fee: {
            fontSize: 28,
            fontWeight: "bold",
            color: "#0072bb",
        },
        itemTime: {
            marginTop: 4,
            fontSize: 12,
            color: "#999"
        }
    }
)

export default ListPage;