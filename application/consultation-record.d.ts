export interface ConsultationRecord {
    recordId: string;
    clinicId: string | undefined;
    doctorName: string;
    patientName: string;
    diagnosis: string;
    medication: string;
    consultationFee: number;
    dateTime: string;
    needFollowup: number;
}