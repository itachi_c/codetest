import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, SafeAreaView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Environment } from './environment';

type LoginState = {
    alertMessage: string;
    loginBody: {
        email: string;
        password: string;
    }

}

class LoginPage extends React.Component<{}, LoginState> {
    constructor(props: {}) {
        super(props)
        this.state = {
            alertMessage: "",
            loginBody: {
                email: "",
                password: "",
            }
        }

    }
    emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/



    register = () => {
        Actions.register();
    }


    checkInput = () => {
        return this.state.loginBody.email.length === 0 || this.state.loginBody.password.length === 0
    }


    updateStateValue = (field: string, value: any) => {
        this.setState(
            {
                loginBody: {
                    ...this.state.loginBody,
                    [field]: value
                }
            }
        )
    }

    login = () => {
        if (this.checkInput()) {
            this.setState({ alertMessage: "Please Enter Login Email and Password" })
        } else {
            if (this.emailReg.test(this.state.loginBody.email)) {


                this.setState({ alertMessage: "" })
                fetch(`http://${Environment.expressUrl}/login`, {
                    method: "POST",
                    headers: {
                        Accept: 'application/json',
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify(this.state.loginBody)

                })
                    .then((response) => response.json())
                    .then((res) => {
                        Actions.list({ userInfo: res });
                    }).catch(error => {
                        this.setState({ alertMessage: typeof error === "string" ? error : "Login Error" })
                    })
            } else {
                this.setState({ alertMessage: "Wrong Email Format" })
            }
        }
    }



    render() {

        return (
            <SafeAreaView style={styles.container}>
                <Text style={styles.title}>Clinic Login</Text>
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder='Login Email'
                        textContentType="username"
                        style={styles.input}
                        onChangeText={text => this.updateStateValue('email', text)}
                        value={this.state.loginBody.email}
                        autoCorrect={false}
                    />
                    <TextInput
                        placeholder='Password'
                        textContentType="password"
                        secureTextEntry={true}
                        style={styles.input}
                        onChangeText={text => this.updateStateValue('password', text)}
                        value={this.state.loginBody.password}
                        autoCorrect={false}
                    />
                    <View style={styles.alertMessageContainer}>
                        <Text style={styles.alertMessage}>
                            {this.state.alertMessage}
                        </Text>
                    </View>

                    <View style={styles.buttonContainer}
                    >
                        <TouchableOpacity
                            style={styles.registerButton}
                            onPress={this.register}
                        >
                            <Text style={styles.buttonText}>
                                Register
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.loginButton}
                            onPress={this.login}
                        >
                            <Text style={styles.buttonText}>
                                Login
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 48,
        fontWeight: "bold"
    },
    inputContainer: {
        width: "60%"
    },
    input: {
        marginTop: 24,
        fontSize: 18,
        height: 32,
        borderWidth: 1,
        paddingHorizontal: 8
    },
    alertMessageContainer: {
        marginTop: 8,
        justifyContent: "flex-start",
    },
    alertMessage: {
        color: "#AA3333",
        fontSize: 12,
    },
    buttonContainer: {
        marginTop: 24,
        flexDirection: "row",
        justifyContent: "space-around"
    },
    registerButton: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        height: 40,
        backgroundColor: "#0072bb",
    },
    loginButton: {
        marginLeft: 16,
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        height: 40,
        backgroundColor: "#347b23",
    },
    buttonText: {
        color: "#fff",
        fontSize: 16
    }
});

export default LoginPage