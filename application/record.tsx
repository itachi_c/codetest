import React, { useState } from 'react';
import { Actions } from 'react-native-router-flux';
import { View, StyleSheet, TextInput, CheckBox, Text, TouchableOpacity, SafeAreaView, Button } from 'react-native';
import { ConsultationRecord } from './consultation-record';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Environment } from './environment';

type RecordProps = {
    record?: ConsultationRecord,
    userInfo: {
        clinicId: string;
        email: string;
        clinicName: string;
        phoneNo: string;
        address: string;
    }
}

type RecordState = {
    record: ConsultationRecord,
    isPickDate: boolean,
}

class RecordPage extends React.Component<RecordProps, RecordState>{
    constructor(props: RecordProps) {
        super(props);
        this.state = {
            record: {
                recordId: this.props.record?.recordId ? this.props.record.recordId : '',
                clinicId: this.props.userInfo.clinicId,
                doctorName: this.props.record?.doctorName ? this.props.record.doctorName : '',
                patientName: this.props.record?.patientName ? this.props.record.patientName : '',
                diagnosis: this.props.record?.diagnosis ? this.props.record.diagnosis : '',
                medication: this.props.record?.medication ? this.props.record.medication : '',
                consultationFee: this.props.record?.consultationFee ? this.props.record.consultationFee : 0,
                dateTime: this.props.record?.dateTime ? this.props.record.dateTime : '',
                needFollowup: this.props.record?.needFollowup ? this.props.record.needFollowup : 0
            },
            isPickDate: false
        }
    }

    updateStateValue = (field: string, value: any) => {
        this.setState(
            {
                record: {
                    ...this.state.record,
                    [field]: value
                }
            }
        )
    }

    followupValue = (value: boolean) => {
        this.setState(
            {
                record: {
                    ...this.state.record,
                    needFollowup: value ? 1 : 0
                }
            }
        )
    }

    postNew = () => {
        fetch(`http://${Environment.expressUrl}/consultation-record`, {
            method: "POST",
            headers: {
                Accept: 'application/json',
                'content-type': 'application/json',
                user_id: this.props.userInfo.clinicId
            },
            body: JSON.stringify(this.state.record)

        })
            .then((res) => {
                if (res) {
                    this.setState(
                        {
                            record: {
                                recordId: "",
                                clinicId: "",
                                doctorName: "",
                                patientName: "",
                                diagnosis: "",
                                medication: "",
                                consultationFee: 0,
                                dateTime: "",
                                needFollowup: 0,

                            }
                        }
                    )
                    Actions.list({ userInfo: this.props.userInfo });
                }
            }).catch(error => {
            })
    }

    onSelectDate = () => {
        this.setState({ isPickDate: true })
    }

    selectDate = (event: any, selectedDate?: Date) => {
        this.setState({ isPickDate: false })
        this.updateStateValue('dateTime', selectedDate?.toISOString().slice(0, 10))
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.inputContainer}>
                    <TextInput
                        editable={typeof this.props.record == "undefined"}
                        placeholder='Doctor Name'
                        style={styles.first}
                        onChangeText={text => this.updateStateValue('doctorName', text)}
                        value={this.state.record.doctorName}
                        autoCorrect={false}
                    />
                    <TextInput
                        editable={typeof this.props.record == "undefined"}
                        placeholder='Patient Name'
                        style={styles.input}
                        onChangeText={text => this.updateStateValue('patientName', text)}
                        value={this.state.record.patientName}
                        autoCorrect={false}
                    />
                    <TextInput
                        multiline={true}
                        numberOfLines={10}
                        editable={typeof this.props.record == "undefined"}
                        placeholder='Diagnosis'
                        style={styles.textbox}
                        onChangeText={text => this.updateStateValue('diagnosis', text)}
                        value={this.state.record.diagnosis}
                        autoCorrect={false}
                    />
                    <TextInput
                        editable={typeof this.props.record == "undefined"}
                        placeholder='Medication'
                        style={styles.input}
                        onChangeText={text => this.updateStateValue('medication', text)}
                        value={this.state.record.medication}
                        autoCorrect={false}
                    />
                    <TextInput
                        editable={typeof this.props.record == "undefined"}
                        placeholder='Consultation Fee'
                        keyboardType="number-pad"
                        style={styles.input}
                        onChangeText={text => this.updateStateValue('consultationFee', text)}
                        value={this.state.record.consultationFee.toString()}
                        autoCorrect={false}
                    />
                    <View style={styles.datePickerContainer}>
                        <TextInput
                            editable={false}
                            placeholder='Date'
                            style={styles.dateInput}
                            value={this.state.record.dateTime ? new Date(this.state.record.dateTime).toDateString() : ""}
                        />
                        {typeof this.props.record === "undefined" ? <Button
                            title={'Select'}
                            onPress={this.onSelectDate}>
                        </Button> : undefined}

                        {this.state.isPickDate ? (<DateTimePicker
                            testID="dateTimePicker"
                            value={new Date()}
                            mode="date"
                            display="default"
                            onChange={this.selectDate}
                        />) : undefined}
                    </View>




                    <View style={styles.checkBoxContainer}>

                        <Text style={styles.checkBoxLabel}>
                            Need Followup
                        </Text>

                        <CheckBox

                            disabled={typeof this.props.record !== "undefined"}
                            value={this.state.record.needFollowup === 1 ? true : false}
                            onValueChange={value => this.followupValue(value)}
                            style={styles.followupCheckBox}
                        />


                    </View>
                </View>
                {
                    typeof this.props.record === "undefined" ?

                        <View style={styles.buttonConainer}>
                            <TouchableOpacity
                                style={styles.submit}
                                onPress={this.postNew}
                            >
                                <Text style={styles.buttonText}>
                                    Submit
                            </Text>
                            </TouchableOpacity>
                        </View> :
                        undefined
                }


            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create(
    {
        container: {
            height: "100%",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "flex-start"
        },
        inputContainer: {
            flex: 1,
            width: "80%"
        },
        first: {
            marginTop: 16,
            height: 32,
            fontSize: 18,
            borderWidth: 1,
            paddingHorizontal: 8
        },
        input: {
            height: 32,
            marginTop: 8,
            fontSize: 18,
            borderWidth: 1,
            paddingHorizontal: 8
        },

        datePickerContainer: {
            height: 32,
            marginTop: 8,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "flex-start"
        },
        dateInput: {
            height: 32,
            fontSize: 18,
            borderWidth: 1,
            paddingHorizontal: 8,
            marginRight: 16

        },
        textbox: {
            marginTop: 8,
            fontSize: 18,
            borderWidth: 1,
            paddingHorizontal: 8,
            paddingVertical: 4
        },
        checkBoxContainer: {
            marginTop: 8,
            height: 32,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "flex-start",
        },
        checkBoxLabel: {
            marginRight: 8
        },
        followupCheckBox: {

        },
        buttonConainer: {
            height: 48,
            width: "100%",
        },
        submit: {
            width: "100%",
            height: "100%",
            backgroundColor: "#347b23",
            justifyContent: "center",
            alignItems: "center"
        },
        buttonText: {
            color: "#fff",
            fontSize: 20,
            fontWeight: "bold",
        }
    }
)

export default RecordPage;