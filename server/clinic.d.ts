export interface Clinic {
    clinicId: string | undefined;
    email: string;
    password: string;
    clinicName: string;
    phoneNo: string;
    address: string;
}