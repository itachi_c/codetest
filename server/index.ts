import express = require('express');
import bodyParser = require('body-parser');
import mysql = require('mysql');
import moment = require("moment");
import cors = require('cors');
const app: express.Application = express();
app.use(cors())

import { ConsultationRecord } from './consultation-record';
import { Clinic } from './clinic';

var jsonParser = bodyParser.json()

const config = {
    host: "localhost",
    user: "root",
    password: "password",
    database: "code_test_clinic"
}

var con = mysql.createConnection(config);


const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/



function _uuid() {
    var d = Date.now();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

function connectDB(MYSQL?: mysql.Connection) {
    if (typeof MYSQL !== "undefined") {
        con.destroy();
        con = MYSQL
    }

    con.connect((err: Error) => {
        if (err) console.log(err);
        else console.log("Connected!");


        app.post('/register', jsonParser, (req: express.Request,
            res: express.Response,
            next: express.NextFunction) => {
            try {
                let registerBody: Clinic = req.body;
                if (emailReg.test(registerBody.email.toString())) {
                    let checkingSql = `SELECT email FROM code_test_clinic.clinic WHERE email="${registerBody.email}"`;
                    con.query(checkingSql, (error, result: Array<any>) => {
                        if (error)
                            res.status(400).send("Error");
                        else if (result && result.length > 0) {
                            res.status(400).send("Account exist");
                        } else {
                            registerBody.clinicId = _uuid();
                            let columns = "";
                            let values = "";

                            for (let key in registerBody) {
                                columns = `${columns}${columns === "" ? '' : ','}${key}`;
                            }
                            for (let item of Object.values(registerBody)) {
                                values = `${values}${values === "" ? '' : ','}"${item}"`;
                            }
                            let registerSql = `INSERT INTO code_test_clinic.clinic (${columns}) VALUES (${values});`

                            con.query(registerSql, (error, result) => {
                                if (error) {
                                    res.status(400).send("Register Fail");
                                } else {
                                    res.status(200).send({ results: "Success" })
                                }
                            })
                        }
                    })
                } else {
                    res.status(400).send(`Worng Email Format`);
                }
            } catch (error) {
                res.status(400).send(`Incomplete Information`);
            }
        })

        app.post('/login', jsonParser, (req: express.Request,
            res: express.Response,
            next: express.NextFunction) => {
            try {
                let loginBody = req.body;
                if (emailReg.test(loginBody.email)) {
                    let loginSql = `SELECT * FROM code_test_clinic.clinic WHERE email="${loginBody.email}" AND password="${loginBody.password}"`
                    con.query(loginSql, (error, result) => {
                        if (error || result.length === 0) {
                            res.status(400).send("Login Fail");
                        } else {
                            delete result[0].password
                            res.status(200).json(result[0])
                        }
                    })
                } else {
                    res.status(400).send(`Worng Email Format`);
                }

            } catch (error) {
                res.status(400).send("Login fail")
            }

        })

        app.post('/consultation-record', jsonParser, (req: express.Request,
            res: express.Response) => {
            try {
                let recordBody: ConsultationRecord = req.body;
                recordBody.recordId = _uuid();
                if (typeof req.headers.user_id === "string") {
                    recordBody.clinicId = req.headers.user_id;
                    let clinicCheckSql = `SELECT * FROM code_test_clinic.clinic WHERE clinicId="${recordBody.clinicId}"`
                    con.query(clinicCheckSql, (error, result: Array<any>) => {
                        if (error) {
                            res.status(400).send("invalid Information");
                        } else if (result && result.length === 0) {
                            res.status(400).send("invalid Clinic");
                        } else {
                            let columns = "";
                            let values = "";
                            recordBody.dateTime = moment(recordBody.dateTime.toString()).format("YYYY-MM-DD HH:mm:ss");
                            recordBody.needFollowup = recordBody.needFollowup ? 1 : 0
                            for (let key in recordBody) {
                                columns = `${columns}${columns === "" ? '' : ','}${key}`;
                            }
                            for (let item of Object.values(recordBody)) {
                                if (typeof item === "string") {
                                    values = `${values}${values === "" ? '' : ','}"${item}"`;
                                } else {
                                    values = `${values}${values === "" ? '' : ','}${item}`;
                                }
                            }
                            let recordSql = `INSERT INTO code_test_clinic.consultation_record (${columns}) VALUES (${values});`
                            con.query(recordSql, (error, result) => {
                                if (error) {
                                    res.status(400).send("Post Record Fail");
                                } else {
                                    res.status(200).json({ results: "Post Record Success" })
                                }
                            })
                        }
                    })
                } else {
                    res.status(400).send("Invalid User ID");
                }
            } catch (error) {
                res.status(400).send("Post Record Fail")
            }

        })

        app.get('/consultation-records', (req: express.Request,
            res: express.Response,
            next: express.NextFunction) => {
            let userId = req.headers.user_id
            if (userId) {

                let recordDateFrom = typeof req.query.recordDateFrom != "undefined" ? req.query.recordDateFrom : null;
                let recordDateTo = typeof req.query.recordDateTo != "undefined" ? req.query.recordDateTo : null;
                let ordering = typeof req.query.ordering != "undefined" ? req.query.ordering : null;
                let offset = typeof req.query.offset != "undefined" ? req.query.offset : 0
                let limit = typeof req.query.limit != "undefined" ? req.query.limit : 10
                let countSql = `SELECT COUNT(recordId) FROM code_test_clinic.consultation_record `;
                let getSql = `SELECT * FROM code_test_clinic.consultation_record `;
                let range = `WHERE clinicId = "${userId}" ` +
                    (recordDateFrom || recordDateTo ? "and " : "") +
                    (recordDateTo ? `dateTime <= '${recordDateTo}'` : "") +
                    (recordDateFrom && recordDateTo ? " and " : "") +
                    (recordDateFrom ? `dateTime >= '${recordDateFrom}'` : "");
                let sorting = " order by " + (ordering ? ` ${ordering} ,` : "dateTime desc ,") + "recordId desc";
                let pagingSql = ` limit ${parseInt(typeof limit != "string" ? limit.toString() : limit)} offset ${typeof offset != "string" ? parseInt(offset.toString()) : offset};`
                con.query(countSql + range + sorting, (countError, count) => {
                    if (countError) {
                        res.status(400).send("Get Record Fail");
                    } else {
                        con.query(getSql + range + sorting + pagingSql, (error, result) => {
                            if (error) {
                                res.status(400).send("Get Record Fail");
                            } else if (result && result.length > 0) {
                                res.setHeader('Content-Type', 'application/json');
                                res.status(200).json(
                                    {
                                        offset: parseInt(offset.toString()),
                                        total: count[0]['COUNT(recordId)'],
                                        results: result
                                    }
                                )
                            } else {
                                res.status(201).json(
                                    {
                                        offset: parseInt(offset.toString()),
                                        total: 0,
                                        results: []
                                    }
                                )
                            }
                        })
                    }
                })

            } else {
                res.status(403).send("Authentication Error")
            }
        })
    });

    con.on('error', (err) => {
        if (!err.fatal) {
            return;
        }
        if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
            throw err;
        }
        console.log('Re-connecting lost connection: ' + err.stack);
        let MYSQL = mysql.createConnection(config);
        connectDB(MYSQL);
    })
}

connectDB();




app.listen(3000, () => {
    console.log("listening localhost:3000");
});