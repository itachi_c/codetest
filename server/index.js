"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var bodyParser = require("body-parser");
var mysql = require("mysql");
var moment = require("moment");
var cors = require("cors");
var app = express();
app.use(cors());
var jsonParser = bodyParser.json();
var config = {
    host: "localhost",
    user: "root",
    password: "password",
    database: "code_test_clinic"
};
var con = mysql.createConnection(config);
var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
function _uuid() {
    var d = Date.now();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}
function connectDB(MYSQL) {
    if (typeof MYSQL !== "undefined") {
        con.destroy();
        con = MYSQL;
    }
    con.connect(function (err) {
        if (err)
            console.log(err);
        else
            console.log("Connected!");
        app.post('/register', jsonParser, function (req, res, next) {
            try {
                var registerBody_1 = req.body;
                if (emailReg.test(registerBody_1.email.toString())) {
                    var checkingSql = "SELECT email FROM code_test_clinic.clinic WHERE email=\"" + registerBody_1.email + "\"";
                    con.query(checkingSql, function (error, result) {
                        if (error)
                            res.status(400).send("Error");
                        else if (result && result.length > 0) {
                            res.status(400).send("Account exist");
                        }
                        else {
                            registerBody_1.clinicId = _uuid();
                            var columns = "";
                            var values = "";
                            for (var key in registerBody_1) {
                                columns = "" + columns + (columns === "" ? '' : ',') + key;
                            }
                            for (var _i = 0, _a = Object.values(registerBody_1); _i < _a.length; _i++) {
                                var item = _a[_i];
                                values = "" + values + (values === "" ? '' : ',') + "\"" + item + "\"";
                            }
                            var registerSql = "INSERT INTO code_test_clinic.clinic (" + columns + ") VALUES (" + values + ");";
                            con.query(registerSql, function (error, result) {
                                if (error) {
                                    res.status(400).send("Register Fail");
                                }
                                else {
                                    res.status(200).send({ results: "Success" });
                                }
                            });
                        }
                    });
                }
                else {
                    res.status(400).send("Worng Email Format");
                }
            }
            catch (error) {
                res.status(400).send("Incomplete Information");
            }
        });
        app.post('/login', jsonParser, function (req, res, next) {
            try {
                var loginBody = req.body;
                if (emailReg.test(loginBody.email)) {
                    var loginSql = "SELECT * FROM code_test_clinic.clinic WHERE email=\"" + loginBody.email + "\" AND password=\"" + loginBody.password + "\"";
                    con.query(loginSql, function (error, result) {
                        if (error || result.length === 0) {
                            res.status(400).send("Login Fail");
                        }
                        else {
                            delete result[0].password;
                            res.status(200).json(result[0]);
                        }
                    });
                }
                else {
                    res.status(400).send("Worng Email Format");
                }
            }
            catch (error) {
                res.status(400).send("Login fail");
            }
        });
        app.post('/consultation-record', jsonParser, function (req, res) {
            try {
                var recordBody_1 = req.body;
                recordBody_1.recordId = _uuid();
                if (typeof req.headers.user_id === "string") {
                    recordBody_1.clinicId = req.headers.user_id;
                    var clinicCheckSql = "SELECT * FROM code_test_clinic.clinic WHERE clinicId=\"" + recordBody_1.clinicId + "\"";
                    con.query(clinicCheckSql, function (error, result) {
                        if (error) {
                            res.status(400).send("invalid Information");
                        }
                        else if (result && result.length === 0) {
                            res.status(400).send("invalid Clinic");
                        }
                        else {
                            var columns = "";
                            var values = "";
                            recordBody_1.dateTime = moment(recordBody_1.dateTime.toString()).format("YYYY-MM-DD HH:mm:ss");
                            recordBody_1.needFollowup = recordBody_1.needFollowup ? 1 : 0;
                            for (var key in recordBody_1) {
                                columns = "" + columns + (columns === "" ? '' : ',') + key;
                            }
                            for (var _i = 0, _a = Object.values(recordBody_1); _i < _a.length; _i++) {
                                var item = _a[_i];
                                if (typeof item === "string") {
                                    values = "" + values + (values === "" ? '' : ',') + "\"" + item + "\"";
                                }
                                else {
                                    values = "" + values + (values === "" ? '' : ',') + item;
                                }
                            }
                            var recordSql = "INSERT INTO code_test_clinic.consultation_record (" + columns + ") VALUES (" + values + ");";
                            con.query(recordSql, function (error, result) {
                                if (error) {
                                    res.status(400).send("Post Record Fail");
                                }
                                else {
                                    res.status(200).json({ results: "Post Record Success" });
                                }
                            });
                        }
                    });
                }
                else {
                    res.status(400).send("Invalid User ID");
                }
            }
            catch (error) {
                res.status(400).send("Post Record Fail");
            }
        });
        app.get('/consultation-records', function (req, res, next) {
            var userId = req.headers.user_id;
            if (userId) {
                var recordDateFrom = typeof req.query.recordDateFrom != "undefined" ? req.query.recordDateFrom : null;
                var recordDateTo = typeof req.query.recordDateTo != "undefined" ? req.query.recordDateTo : null;
                var ordering = typeof req.query.ordering != "undefined" ? req.query.ordering : null;
                var offset_1 = typeof req.query.offset != "undefined" ? req.query.offset : 0;
                var limit = typeof req.query.limit != "undefined" ? req.query.limit : 10;
                var countSql = "SELECT COUNT(recordId) FROM code_test_clinic.consultation_record ";
                var getSql_1 = "SELECT * FROM code_test_clinic.consultation_record ";
                var range_1 = "WHERE clinicId = \"" + userId + "\" " +
                    (recordDateFrom || recordDateTo ? "and " : "") +
                    (recordDateTo ? "dateTime <= '" + recordDateTo + "'" : "") +
                    (recordDateFrom && recordDateTo ? " and " : "") +
                    (recordDateFrom ? "dateTime >= '" + recordDateFrom + "'" : "");
                var sorting_1 = " order by " + (ordering ? " " + ordering + " ," : "dateTime desc ,") + "recordId desc";
                var pagingSql_1 = " limit " + parseInt(typeof limit != "string" ? limit.toString() : limit) + " offset " + (typeof offset_1 != "string" ? parseInt(offset_1.toString()) : offset_1) + ";";
                con.query(countSql + range_1 + sorting_1, function (countError, count) {
                    if (countError) {
                        res.status(400).send("Get Record Fail");
                    }
                    else {
                        con.query(getSql_1 + range_1 + sorting_1 + pagingSql_1, function (error, result) {
                            if (error) {
                                res.status(400).send("Get Record Fail");
                            }
                            else if (result && result.length > 0) {
                                res.setHeader('Content-Type', 'application/json');
                                res.status(200).json({
                                    offset: parseInt(offset_1.toString()),
                                    total: count[0]['COUNT(recordId)'],
                                    results: result
                                });
                            }
                            else {
                                res.status(201).json({
                                    offset: parseInt(offset_1.toString()),
                                    total: 0,
                                    results: []
                                });
                            }
                        });
                    }
                });
            }
            else {
                res.status(403).send("Authentication Error");
            }
        });
    });
    con.on('error', function (err) {
        if (!err.fatal) {
            return;
        }
        if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
            throw err;
        }
        console.log('Re-connecting lost connection: ' + err.stack);
        var MYSQL = mysql.createConnection(config);
        connectDB(MYSQL);
    });
}
connectDB();
app.listen(3000, function () {
    console.log("listening localhost:3000");
});
